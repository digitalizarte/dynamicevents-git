Local lcCode As String, ;
	lcCode2 As String, ;
	lcCommand As String, ;
	lcFile As String, ;
	lcFile3 As String, ;
	loForm As oFrmDynamic Of frmeventhandler.prg, ;
	loXA As Xmladapter

On Shutdown Quit
Close Data All

Set Path To ( Justpath ( Sys ( 16 ) ) ) Additive

lcFile = Addbs ( Getenv ( 'TEMP' ) ) + Forceext ( Sys ( 2015 ), 'xcfg' )

Use In Select ( 'cfgControls' )
Create Cursor cfgControls ( cObjName Varchar ( 50 ), ;
	cParent Varchar ( 50 ), ;
	cClass Varchar ( 50 ), ;
	cClassLib Varchar ( 50 ), ;
	cProps Memo, ;
	cEventCode Memo, ;
	loaded l )

Use In Select ( 'cfgMethods' )
Create Cursor cfgMethods ( cObjName Varchar ( 50 ), ;
	cEventName Varchar ( 50 ), ;
	cEventCode Memo, ;
	nPriority i )

Use In Select ( 'cfgProperties' )
Create Cursor cfgProperties ( cObjName Varchar ( 50 ), ;
	cProps Memo )

Use In Select ( 'cfgConfig' )
Create Cursor cfgConfig ( cObjName Varchar ( 50 ), ;
	cParent Varchar ( 50 ), ;
	cClass Varchar ( 50 ), ;
	cClassLib Varchar ( 50 ), ;
	cProps Memo, ;
	loaded l, ;
	cEventName Varchar ( 50 ), ;
	cEventCode Memo, ;
	nPriority i, ;
	cType Char ( 1 ) )

TEXT To lcCode Noshow Textmerge Pretext 1 + 2 + 4
	Visible = .T.
	Caption = '\<Salir'
	Top = 8
	Left = 108

ENDTEXT

TEXT To lcCode2 Noshow Textmerge Pretext 1 + 2 + 4
	ThisForm.Release()

ENDTEXT

Insert Into cfgConfig ;
	( cObjName, cParent, cClass, cClassLib, cProps, loaded, cType, cEventName, cEventCode ) ;
	Values ( 'cmdSalir', '', 'commandbutton', '', lcCode, .F., 'C', 'Click', lcCode2 )

lcFile3 = Forceext ( lcFile, 'dcfg' )

TEXT To lcCommand Noshow Textmerge Pretext 15
	Select  *
		From cfgConfig
		Into Table '<<lcFile3>>'

ENDTEXT

&lcCommand.

Use In Select ( 'cfgConfig' )

Select cfgControls
Locate

TEXT To lcCode Noshow Textmerge Pretext 1 + 2 + 4
	Visible = .T.
	Caption = '\<Salir'
	Top = 8
	Left = 216

ENDTEXT

Insert Into cfgControls ;
	( cObjName, cParent, cClass, cClassLib, cProps, loaded ) ;
	Values ( 'cmdSalir', 'cntVarios', 'commandbutton', '', lcCode, .F. )

TEXT To lcCode Noshow Textmerge Pretext 1 + 2 + 4
	Visible = .T.
	Caption = '\<Aceptar'
	Top = 8
	Left = 8

ENDTEXT

Insert Into cfgControls ;
	( cObjName, cParent, cClass, cClassLib, cProps, loaded ) ;
	Values ( 'cmdAceptar', 'cntVarios', 'commandbutton', '', lcCode, .F. )

TEXT To lcCode Noshow Textmerge Pretext 1 + 2 + 4
	Visible = .T.
	Caption = '\<Test'
	Top = 8
	Left = 108

ENDTEXT

Insert Into cfgControls ;
	( cObjName, cParent, cClass, cClassLib, cProps, loaded ) ;
	Values ( 'cmdTest', 'cntVarios', 'commandbutton', '', lcCode, .F. )

TEXT To lcCode Noshow Textmerge Pretext 1 + 2 + 4
	Visible = .T.
	Top = 30
	Left = 8
	Width = 324
	Height = 50
	borderWidth = 1
	BorderColor = Rgb( 255, 0, 0 )

ENDTEXT

Insert Into cfgControls ;
	( cObjName, cParent, cClass, cClassLib, cProps, loaded ) ;
	Values ( 'cntVarios', '', 'Container', '', lcCode, .F. )

TEXT To lcCode Noshow Textmerge Pretext 1 + 2 + 4
	Visible = .T.
	Top = 150
	Left = 8
	Width = 324
	Height = 50
	borderWidth = 1
	BorderColor = Rgb( 0, 0, 255 )

ENDTEXT

Insert Into cfgControls ;
	( cObjName, cParent, cClass, cClassLib, cProps, loaded ) ;
	Values ( 'cntVarios2', '', 'ContainerBase', 'DynamicEvents.prg', lcCode, .F. )

TEXT To lcCode Noshow Textmerge Pretext 1 + 2 + 4
	This.MousePointer = 0

ENDTEXT

Insert Into cfgMethods ( cObjName, cEventName, cEventCode, nPriority ) ;
	Values ( 'cntVarios2', 'MouseLeave', lcCode, 0 )

TEXT To lcCode Noshow Textmerge Pretext 1 + 2 + 4
	This.MousePointer = 15

ENDTEXT

Insert Into cfgMethods ( cObjName, cEventName, cEventCode, nPriority ) ;
	Values ( 'cntVarios2', 'MouseEnter', lcCode, 0 )

TEXT To lcCode Noshow Textmerge Pretext 1 + 2 + 4
	Thisform.Pset( Mcol( Thisform.name, 3 ), Mrow( Thisform.name, 3 ) )
	Thisform.ForeColor = Rgb( Rand() * 255, Rand() * 255, Rand() * 255 )

ENDTEXT

Insert Into cfgMethods ( cObjName, cEventName, cEventCode, nPriority ) ;
	Values ( 'cntVarios2', 'Click', lcCode, 0 )

TEXT To lcCode Noshow Textmerge Pretext 1 + 2 + 4
	ThisForm.Pset( Mcol( Thisform.name, 3 ), Mrow( Thisform.name, 3 ) )
	ThisForm.ForeColor = Rgb( Rand() * 255, Rand() * 255, Rand() * 255 )

ENDTEXT

Insert Into cfgMethods ( cObjName, cEventName, cEventCode, nPriority ) ;
	Values ( 'cntVarios2', 'MouseMove', lcCode, 0 )

TEXT To lcCode Noshow Textmerge Pretext 1 + 2 + 4
	If Thisform.WindowType = 0 && If Modeless, Release the form.
		Thisform.Release()

	Else
		Thisform.Hide() && If Modal, just hide the form. Calling code should release it when ready to do so.

	Endif
	Clear Events

ENDTEXT

Insert Into cfgMethods ( cObjName, cEventName, cEventCode, nPriority ) ;
	Values ( 'cmdsalir', 'click', lcCode, 0 )

TEXT To lcCode Noshow Textmerge Pretext 1 + 2 + 4
	MessageBox( ThisformSet.FormCount )

ENDTEXT

Insert Into cfgMethods ( cObjName, cEventName, cEventCode, nPriority ) ;
	Values ( 'cmdTest', 'click', lcCode, 0 )

TEXT To lcCode Noshow Textmerge Pretext 1 + 2 + 4
	Thisform.BackColor = Rgb( Rand() * 255, Rand() * 255, Rand() * 255 )

ENDTEXT

Insert Into cfgMethods ( cObjName, cEventName, cEventCode, nPriority ) ;
	Values ( 'cmdaceptar', 'click', lcCode, 0 )

TEXT To lcCode Noshow Textmerge Pretext 1 + 2 + 4
	BackColor = Rgb( 0, 0 ,255 )

ENDTEXT
Insert Into cfgProperties ( cObjName, cProps ) ;
	Values ( 'cmdaceptar', lcCode )

loXA = Createobject ( 'XMLAdapter' )
loXA.AddTableSchema ( 'cfgControls' )
loXA.AddTableSchema ( 'cfgMethods' )
loXA.AddTableSchema ( 'cfgProperties' )

loXA.ToXML ( lcFile, , .T. )

loXA = Null

Use In Select ( 'cfgControls' )
Use In Select ( 'cfgProperties' )
Use In Select ( 'cfgMethods' )

If Version ( 2 ) = 0
	loForm = Newobject ( 'DynamicEventsForm', 'DynamicEvents.prg', '' )

Else
	loForm = Newobject ( 'DynamicEventsForm', Locfile ( 'DynamicEvents.prg' ), '' )

Endif

* XML
loForm.LoadConfig ( lcFile )
* Modify Command ( lcFile ) Nowait
loForm.Closable =  .F.

#Define Run
loForm.Show ( 0 )

Read Events

loForm = Null
