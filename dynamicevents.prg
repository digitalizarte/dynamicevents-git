#Define CR Chr( 13 )
#Define TB Chr( 9 )

* DynamicEventsHandler
Define Class DynamicEventsHandler As Form

	#If .F.
		Local This As DynamicEventsHandler Of DynamicEvents.prg
	#Endif

	_MemberData = [<?xml version="1.0" encoding="Windows-1252" standalone="yes"?>] + ;
		[<VFPData>] + ;
		[<memberdata name="lonexit" type="property" display="lOnExit" />] + ;
		[<memberdata name="ocolevents" type="property" display="oColEvents" />] + ;
		[<memberdata name="ocolevents_access" type="method" display="oColEvents_Access" />] + ;
		[<memberdata name="ocoldefaultparameters" type="property" display="oColDefaultParameters" />] + ;
		[<memberdata name="ocoldefaultparameters_access" type="method" display="oColDefaultParameters_Access" />] + ;
		[<memberdata name="addeventcode" type="method" display="AddEventCode" />] + ;
		[<memberdata name="addorgetevent" type="method" display="AddOrGetEvent" />] + ;
		[<memberdata name="addorgetcontrol" type="method" display="AddOrGetControl" />] + ;
		[<memberdata name="addcode" type="method" display="AddCode" />] + ;
		[<memberdata name="preparecode" type="method" display="PrepareCode" />] + ;
		[<memberdata name="getpath" type="method" display="GetPath" />] + ;
		[<memberdata name="getref" type="method" display="GetRef" />] + ;
		[<memberdata name="getrefr" type="method" display="GetRefR" />] + ;
		[<memberdata name="handlerevent" type="method" display="HandlerEvent" />] + ;
		[<memberdata name="getoriginalerror" type="method" display="GetOriginalError" />] + ;
		[<memberdata name="getexceptionstring" type="method" display="GetExceptionString" />] + ;
		[<memberdata name="loadconfig" type="method" display="LoadConfig" />] + ;
		[<memberdata name="loadcontrols" type="method" display="LoadControls" />] + ;
		[<memberdata name="loadproperties" type="method" display="LoadProperties" />] + ;
		[<memberdata name="loadmethods" type="method" display="LoadMethods" />] + ;
		[</VFPData>]

	Add Object Protected oTimer As Timer With Interval = 5, Enable = .F.

	Protected oThreads
	oThreads = Null

	Protected lOnExit
	lOnExit = .F.

	Protected oColEvents
	oColEvents = Null

	Protected oColDefaultParameters
	oColDefaultParameters = Null

	Protected oColTempEventCodeFiles
	oColTempEventCodeFiles = Null

	Protected cTempPath
	cTempPath = ''

	Protected cSetProc
	cSetProc = ''

	xReturn = .F.

	oColFileToCompile = Null
	oColFileCompileStatus = Null

	nCompileIndex = 0

	* oTimer_Timer
	Procedure oTimer.Timer()
		Local lcFile As String, ;
			lcTalk As String

		Thisform.nCompileIndex = Thisform.nCompileIndex + 1
		If  Thisform.nCompileIndex <= Thisform.oColFileToCompile.Count
			lcTalk = Set ('Talk')
			Set Talk Off
			lcFile = Thisform.oColFileToCompile[ Thisform.nCompileIndex ]
			If ! File( Forceext( lcFile, 'fxp' ) )
				Compile ( lcFile ) Encrypt Nodebug 

			Endif && ! File( Forceext( lcFile, 'fxp' ) )
			loStatus = Thisform.oColFileCompileStatus.Item[ lcFile ]
			loStatus.lCompiled = .T.

			Set Procedure To ( lcFile ) Additive
			DoEvents
			Set Talk &lcTalk.

		Else
			This.Interval = 0
			This.Enable = .F.
			* Thisform.LockScreen = .F.

		Endif

	Endproc && oTimer_Timer

	* cTempPath_Access
	Protected Procedure cTempPath_Access() As String

		If Empty ( This.cTempPath )
			This.cTempPath = This.GetTempPath()

		Endif

		Return This.cTempPath

	Endproc && cTempPath_Access

	* AddCode
	Protected Procedure  AddCode ( toControl As Collection, tcCode As String, tnPrioridad As Number )

		Local lcKey As String, ;
			lcKeyNew As String, ;
			liIdx As Integer, ;
			liJdx As Integer, ;
			loErr As Exception, ;
			loError As Exception

		Try

			If Vartype ( tnPrioridad ) # 'N' Or Empty ( tnPrioridad )
				tnPrioridad = 99

			Endif && Vartype ( tnPrioridad ) # 'N' Or Empty ( tnPrioridad )

			tnPrioridad = Max ( Min ( tnPrioridad, 99 ), 1 )
			lcKey = Transform ( tnPrioridad )
			liIdx = toControl.GetKey ( lcKey )
			If Empty ( liIdx )
				If toControl.Count > 0
					toControl.Add ( tcCode, lcKey, lcKey )

				Else
					toControl.Add ( tcCode, lcKey )

				Endif
			Else
				liJdx = 0
				lcKeyNew = lcKey + Chr ( 64 + liJdx )
				liIdx = toControl.GetKey ( lcKeyNew )
				Do While ! Empty ( liIdx )
					lcKeyNew = lcKey + Chr ( 64 + liJdx )
					liIdx = toControl.GetKey ( lcKeyNew )
					liJdx = liJdx + 1

				Enddo
				toControl.Add ( tcCode, lcKeyNew )

			Endif && Empty( liIdx )

		Catch To loErr
			loError = This.GetOriginalError ( loErr )
			loError.Message = 'AddCode' + CR + loError.Message
			Throw loError

		Finally
			loError = Null

		Endtry

	Endproc  && AddCode

	* AddEventCode
	Procedure AddEventCode ( tcObject As String, tcEvent As String, tcCode As String, tnPrioridad As Number )

		Local lcCode As String, ;
			lcFile As String, ;
			lcHash As String, ;
			lcMessage As String, ;
			lcObjPath As String, ;
			lcSafety As String, ;
			llOk As Boolean, ;
			loControl As Collection, ;
			loErr As Exception, ;
			loError As Exception, ;
			loEvent As Collection, ;
			loObject As Object

		Try
			lcSafety = Set ( 'Safety' )
			tcEvent = Alltrim ( tcEvent )

			* Obtiene la referencia del control.
			loObject = This.GetRef ( tcObject )

			* Verifica que el control tiene el evento.
			If Pemstatus ( loObject, tcEvent, 5 )
				llOk = .T.

				lcObjPath = This.GetPath ( loObject )

				lcHash = MD5 ( tcCode )

				TEXT To lcFile Textmerge Noshow Pretext 15
					<<Addbs( This.cTempPath )>>DE<<lcHash>>.prg
				ENDTEXT

				* Verifica si existe el codigo.
				If ! File ( lcFile ) And ! File ( Forceext( lcFile, 'fxp' ) )
					* Prepara el codigo para grabarlo en el archivo.
					lcCode = This.PrepareCode ( tcEvent, tcCode )
					Set Safety Off
					Strtofile ( lcCode, lcFile, 0 )
					This.oColTempEventCodeFiles.Add ( lcFile, lcFile )
					This.oColFileToCompile.Add ( lcFile )
					This.oColFileCompileStatus.Add( CreateObjParam( 'cFile', lcFile, 'lCompiled', .F. ), lcFile )
					This.oTimer.Enabled =  .T.

				Else
					liIdx = This.oColFileCompileStatus.GetKey( lcFile )
					If Empty( liIdx )
						This.oColFileCompileStatus.Add( CreateObjParam( 'cFile', lcFile, 'lCompiled', .T. ), lcFile )

					Endif && Empty( liIdx )

				Endif && ! File ( lcFile )

				* Obtiene el evento.
				loEvent = This.AddOrGetEvent ( tcEvent )

				* Obtiene el control.
				loControl = This.AddOrGetControl ( loEvent, lcObjPath )

				* Enlaza el evento con el control con la prioridad dada.
				This.BindCode ( loControl, lcFile, tnPrioridad )

				Bindevent ( loObject, tcEvent, Thisform, 'HandlerEvent', 1 )

			Endif && PemStatus( loObject, tcEvent, 5 )

		Catch To loErr

			loError = This.GetOriginalError ( loErr )

			loError.Message = 'AddEventCode' + CR ;
				+ TB + 'tcObject: ' + tcObject + CR ;
				+ TB + 'tcEvent: ' + tcEvent + CR ;
				+ TB + 'tcCode: ' + tcCode + CR + loError.Message

			lcMessage = This.GetExceptionString ( loError )

			If Version ( 2 ) # 2
				Strtofile ( CR + lcMessage, Forceext ( This.Name, 'Err' ), 1 )

			Else
				Messagebox ( lcMessage )

			Endif && Version( 2 ) # 2

			llOk = .F.

		Finally
			loError = Null
			Set Safety &lcSafety.

		Endtry

		Return llOk

	Endproc && AddEventCode

	* AddOrGetControl
	Protected Function AddOrGetControl ( toEvent As Object, tcControl As String ) As Object

		Local lcKey As String, ;
			liIdx As Integer, ;
			loControl As Collection, ;
			loErr As Object, ;
			loError As Exception

		Try
			lcKey = Lower ( tcControl )
			liIdx = toEvent.GetKey ( lcKey )
			If Empty ( liIdx )
				loControl = Createobject ( 'Collection' )
				toEvent.Add ( loControl, lcKey )

			Else && Empty( liIdx )
				loControl = toEvent.Item ( liIdx )

			Endif && Empty( liIdx )

		Catch To loErr
			loError = This.GetOriginalError ( loErr )
			loError.Message = 'AddOrGetControl' + CR + loError.Message
			Throw loError

		Finally
			loError = Null

		Endtry

		Return loControl

	Endfunc && AddOrGetControl

	* AddOrGetEvent
	Protected Function AddOrGetEvent ( tcEvent As String ) As Object

		Local lcKey As String, ;
			liIdx As Integer, ;
			loErr As Object, ;
			loError As Exception, ;
			loEvent As Collection

		Try

			lcKey = Lower ( tcEvent )
			liIdx = This.oColEvents.GetKey ( lcKey )
			If Empty ( liIdx )
				loEvent = Createobject ( 'Collection' )
				This.oColEvents.Add ( loEvent, lcKey )

			Else && Empty( liIdx )
				loEvent = This.oColEvents.Item ( liIdx )

			Endif && Empty( liIdx )

		Catch To loErr
			loError = This.GetOriginalError ( loErr )
			loError.Message = 'AddOrGetEvent' + CR + loError.Message
			Throw loError

		Finally
			loError = Null

		Endtry

		Return loEvent

	Endfunc && AddOrGetEvent

	* BindCode
	Protected Procedure  BindCode ( toControl As Collection, tcFile As String, tnPrioridad As Number )

		Local lcKey As String, ;
			lcKeyNew As String, ;
			liIdx As Integer, ;
			liJdx As Integer, ;
			loErr As Exception, ;
			loError As Exception

		Try

			If Vartype ( tnPrioridad ) # 'N' Or Empty ( tnPrioridad )
				tnPrioridad = 99

			Endif && Vartype ( tnPrioridad ) # 'N' Or Empty ( tnPrioridad )

			tnPrioridad = Max ( Min ( tnPrioridad, 99 ), 1 )
			lcKey = Transform ( tnPrioridad )
			liIdx = toControl.GetKey ( lcKey )
			If Empty ( liIdx )
				If toControl.Count > 0
					toControl.Add ( tcFile, lcKey, lcKey )

				Else && toControl.Count > 0
					toControl.Add ( tcFile, lcKey )

				Endif && toControl.Count > 0

			Else && Empty ( liIdx )
				liJdx = 0
				lcKeyNew = lcKey + Chr ( 64 + liJdx )
				liIdx = toControl.GetKey ( lcKeyNew )
				Do While ! Empty ( liIdx )
					lcKeyNew = lcKey + Chr ( 64 + liJdx )
					liIdx = toControl.GetKey ( lcKeyNew )
					liJdx = liJdx + 1

				Enddo
				toControl.Add ( tcFile, lcKeyNew )

			Endif && Empty( liIdx )

		Catch To loErr
			loError = This.GetOriginalError ( loErr )
			loError.Message = 'BindCode' + CR + loError.Message
			Throw loError

		Finally
			loError = Null

		Endtry

	Endproc  && BindCode

	* ClearBindEvents
	Protected Procedure ClearBindEvents() As VOID

		Local lcControl As String, ;
			lcEvent As String, ;
			liIdx As Integer, ;
			liJdx As Integer, ;
			loColEvents As Collection, ;
			loControl As Collection, ;
			loEvent As Collection, ;
			loObj As Object

		This.lOnExit = .T.

		loColEvents = This.oColEvents
		For liIdx = loColEvents.Count To 1 Step - 1
			loEvent = loColEvents.Item ( liIdx )
			lcEvent = loColEvents.GetKey ( liIdx )

			For liJdx = loEvent.Count To 1 Step - 1
				loControl = loEvent.Item ( liJdx )
				lcControl = loEvent.GetKey ( liJdx )

				* Obtiene la referencia del control.
				loObj = &lcControl.

				* Elimina el enlace del evento.
				Unbindevents ( loObj, lcEvent, Thisform, 'HandlerEvent' )

				* Elimina los codigos asociados al evento y al control
				loControl.Remove ( - 1 )

				* Elimino el control del evento
				loEvent.Remove ( liJdx )

			Endfor

			* Elimino el evento
			loColEvents.Remove ( liIdx )

		Endfor

		This.oColEvents.Remove ( - 1 )

		Unbindevents ( This )

	Endproc && ClearBindEvents

	* Destroy
	Procedure Destroy()
		Local lcFile As String, ;
			lcSetProc As String

		This.lOnExit = .T.
		This.ClearBindEvents()
		lcSetProc = This.cSetProc

		Set Procedure To &lcSetProc.

		For Each lcFile In This.oColTempEventCodeFiles FoxObject
			Delete File ( lcFile )
			* Delete File ( Forceext( lcFile, 'fxp' ) )

		Next

		This.oColDefaultParameters = Null
		This.oColTempEventCodeFiles = Null
		This.oColFileToCompile = Null
		This.oColFileCompileStatus = Null

		DoDefault()

	Endproc && Destroy

	* FillDefaultParameters
	* Cargo los nombres de los parametros de los eventos mas comunes
	Protected Procedure FillDefaultParameters() As VOID

		Local loCol As Collection, ;
			loErr As Exception, ;
			loError As Exception

		Try

			With This As oForm Of Pruebas\EditFrm.prg

				* Drag Events
				* DragDrop
				loCol = Createobject ( 'Collection' )
				loCol.Add ( 'oSource' )
				loCol.Add ( 'nXCoord' )
				loCol.Add ( 'nYCoord' )
				This.oColDefaultParameters.Add ( loCol, 'dragdrop' )

				* DragOver
				loCol = Createobject ( 'Collection' )
				loCol.Add ( 'oSource' )
				loCol.Add ( 'nXCoord' )
				loCol.Add ( 'nYCoord' )
				loCol.Add ( 'nState' )
				This.oColDefaultParameters.Add ( loCol, 'dragover' )

				* Error
				loCol = Createobject ( 'Collection' )
				loCol.Add ( 'nError' )
				loCol.Add ( 'cMethod' )
				loCol.Add ( 'nLine' )
				This.oColDefaultParameters.Add ( loCol, 'error' )

				* KeyPress
				loCol = Createobject ( 'Collection' )
				loCol.Add ( 'nKeyCode' )
				loCol.Add ( 'nShiftAltCtrl' )
				This.oColDefaultParameters.Add ( loCol, 'keypress' )

				* Mouse Events
				loCol = Createobject ( 'Collection' )
				loCol.Add ( 'nButton' )
				loCol.Add ( 'nShift' )
				loCol.Add ( 'nXCoord' )
				loCol.Add ( 'nYCoord' )
				This.oColDefaultParameters.Add ( loCol, 'mousedown' )
				This.oColDefaultParameters.Add ( loCol, 'mouseenter' )
				This.oColDefaultParameters.Add ( loCol, 'mouseleave' )
				This.oColDefaultParameters.Add ( loCol, 'mousemove' )
				This.oColDefaultParameters.Add ( loCol, 'mouseup' )

				* Mouse Events : MouseWheel
				loCol = Createobject ( 'Collection' )
				loCol.Add ( 'nDirection' )
				loCol.Add ( 'nShift' )
				loCol.Add ( 'nXCoord' )
				loCol.Add ( 'nYCoord' )
				This.oColDefaultParameters.Add ( loCol, 'mousewheel' )

				* OLE Events
				* OLECompleteDrag
				loCol = Createobject ( 'Collection' )
				loCol.Add ( 'nEffect' )
				This.oColDefaultParameters.Add ( loCol, 'olecompletedrag' )

				* OLEDragDrop
				loCol = Createobject ( 'Collection' )
				loCol.Add ( 'oDataObject' )
				loCol.Add ( 'nEffect' )
				loCol.Add ( 'nButton' )
				loCol.Add ( 'nShift' )
				loCol.Add ( 'nXCoord' )
				loCol.Add ( 'nYCoord' )
				This.oColDefaultParameters.Add ( loCol, 'oledragdrop' )

				* OLEDragOver
				loCol = Createobject ( 'Collection' )
				loCol.Add ( 'oDataObject' )
				loCol.Add ( 'nEffect' )
				loCol.Add ( 'nButton' )
				loCol.Add ( 'nShift' )
				loCol.Add ( 'nXCoord' )
				loCol.Add ( 'nYCoord' )
				loCol.Add ( 'nState' )
				This.oColDefaultParameters.Add ( loCol, 'oledragover' )

				* OLEGiveFeedback
				loCol = Createobject ( 'Collection' )
				loCol.Add ( 'nEffect' )
				loCol.Add ( 'eMouseCursor' )
				This.oColDefaultParameters.Add ( loCol, 'olegivefeedback' )

				* OLESetData
				loCol = Createobject ( 'Collection' )
				loCol.Add ( 'oDataObject' )
				loCol.Add ( 'eFormat' )
				This.oColDefaultParameters.Add ( loCol, 'olesetdata' )

				* OLEStartDrag
				loCol = Createobject ( 'Collection' )
				loCol.Add ( 'oDataObject' )
				loCol.Add ( 'nEffect' )
				This.oColDefaultParameters.Add ( loCol, 'olestartdrag' )

				* OnMoveItem
				loCol = Createobject ( 'Collection' )
				loCol.Add ( 'nSource' )
				loCol.Add ( 'nShift' )
				loCol.Add ( 'nCurrentIndex' )
				loCol.Add ( 'nMoveBy' )
				This.oColDefaultParameters.Add ( loCol, 'onmoveitem' )

				* Scrolled
				loCol = Createobject ( 'Collection' )
				loCol.Add ( 'nDirection' )
				This.oColDefaultParameters.Add ( loCol, 'scrolled' )

				* UIEnable
				loCol = Createobject ( 'Collection' )
				loCol.Add ( 'lEnable' )
				This.oColDefaultParameters.Add ( loCol, 'uienable' )

			Endwith

		Catch To loErr
			loError = This.GetOriginalError ( loErr )
			loError.Message = 'FillDefaultParameters' + CR + loError.Message
			Throw loError

		Finally
			loCol = Null
			loError = Null

		Endtry

	Endproc && FillDefaultParameters

	* GetExceptionString
	Protected Procedure GetExceptionString ( toErr As Exception ) As String

		Local laStackInfo[1], ;
			lcProc As String, ;
			lcRet As String, ;
			lnCnt As Integer

		lcProc = 'Procedure'

		TEXT To lcRet Noshow Textmerge Pretext 1 + 2 + 4
			When: <<Dtoc( Date(), 1 )>> <<Time( 0 )>>
			Details: <<Alltrim( toErr.Details )>>
			Message: <<Alltrim( toErr.Message )>>
			LineNo: <<Transform( toErr.Lineno )>>
			LineContents: <<Alltrim( toErr.LineContents )>>
			ErrorNo: <<Transform( toErr.ErrorNo )>>
			<<lcProc>>: <<Alltrim( toErr.Procedure )>>
			StackLevel: <<Transform( toErr.StackLevel )>>
		ENDTEXT

		Return lcRet

	Endproc && GetExceptionString

	* GetOriginalError
	Protected Procedure GetOriginalError ( toErr As Exception @ ) As Exception

		Local loError As Exception

		loError = toErr
		Do While Vartype ( loError.UserValue ) == 'O'
			loError = loError.UserValue

		Enddo

		Return loError

	Endproc && GetOriginalError

	* GetPath
	Protected Function GetPath ( toObj As Object ) As String

		Local lcRet As String, ;
			loErr As Object, ;
			loError As Exception, ;
			loObj As Object

		lcRet = ''

		Try
			If Vartype ( toObj ) = 'O'

				If ! Pemstatus ( toObj, '__FullObjectPath', 5 )
					Do While ! Isnull ( toObj ) And Lower ( toObj.BaseClass ) # 'form'
						lcRet = toObj.Name + '.' + lcRet
						toObj = toObj.Parent

					Enddo

					If ! Empty ( lcRet )
						lcRet = 'ThisForm.' + lcRet
						If Right ( lcRet, 1 ) = '.'
							lcRet = Left ( lcRet, Len ( lcRet ) - 1 )

						Endif && Right( lcRet, 1 ) = '.'

					Else
						lcRet = 'ThisForm'

					Endif && ! Empty( lcRet )

					AddProperty ( toObj, '__FullObjectPath', lcRet )

				Else
					lcRet = toObj.__FullObjectPath

				Endif && ! Pemstatus( toObj, '__FullObjectPath', 5 )

			Endif && Vartype( toObj ) = 'O'

		Catch To loErr
			loError = This.GetOriginalError ( loErr )
			loError.Message = 'GetPath' + CR ;
				+ TB + 'toObj: ' + toObj.Name + CR + loError.Message
			Throw loError

		Finally
			loError = Null

		Endtry

		Return lcRet

	Endfunc && GetPath

	* GetRef
	Protected Function GetRef ( tcObj As String ) As Object

		Local loErr As Object, ;
			loError As Exception, ;
			loObj As Object

		Try
			tcObj = Alltrim ( tcObj )

			If Atc ( '.', tcObj ) > 0
				loObj = &tcObj

			Else
				If Lower ( tcObj ) $ 'this thisform'
					loObj = This

				Else
					loObj = This.GetRefR ( tcObj, This )

				Endif && Lower( tcObj ) $ 'this thisform'

			Endif && Atc( '.', tcObj ) > 0

		Catch To loErr
			loObj = Null
			loError = This.GetOriginalError ( loErr )
			loError.Message = 'GetRef' + CR ;
				+ TB + 'tcObj: ' + tcObj + CR + loErr.Message

			Throw loError

		Finally
			loError = Null

		Endtry

		Return loObj

	Endfunc && GetRef

	* GetRefR
	Protected Function GetRefR ( tcObj As String, toObj As Object ) As Object

		Local lcBaseClass As String, ;
			lnCnt As Number, ;
			loControl As Object, ;
			loErr As Object, ;
			loError As Exception, ;
			loObj As Object

		Try

			loObj = Null
			tcObj = Lower ( Alltrim ( tcObj ) )

			If Vartype ( toObj ) = 'O'
				lcBaseClass = Lower ( toObj.BaseClass )
				Do Case
					Case lcBaseClass $ 'form page container'
						lnCnt = 1
						Do While Vartype ( loObj ) # 'O' And lnCnt <= toObj.Objects.Count
							loControl = toObj.Objects[ lnCnt ]
							If Pemstatus ( loControl, 'Name', 5 ) And Lower ( loControl.Name ) == tcObj
								loObj = loControl

							Else
								loObj = This.GetRefR ( tcObj, loControl )

							Endif && Pemstatus( loControl, 'Name', 5 ) And Lower( loControl.Name ) == tcObj
							lnCnt = lnCnt + 1

						Enddo

					Case lcBaseClass = 'pageframe'
						lnCnt = 1
						Do While Vartype ( loObj ) # 'O' And lnCnt <= toObj.Pages.Count
							loControl = toObj.Pages[ lnCnt ]
							If Pemstatus ( loControl, 'Name', 5 ) And Lower ( loControl.Name ) == tcObj
								loObj = loControl

							Else
								loObj = This.GetRefR ( tcObj, loControl )

							Endif && Pemstatus( loControl, 'Name', 5 ) And Lower( loControl.Name ) == tcObj
							lnCnt = lnCnt + 1

						Enddo

					Case lcBaseClass == 'grid'
						lnCnt = 1
						Do While Vartype ( loObj ) # 'O' And lnCnt <= toObj.Columns.Count
							loControl = toObj.Columns[ lnCnt ]
							If Pemstatus ( loControl, 'Name', 5 ) And Lower ( loControl.Name ) == tcObj
								loObj = loControl

							Else
								loObj = This.GetRefR ( tcObj, loControl )

							Endif
							lnCnt = lnCnt + 1

						Enddo

					Otherwise

						If Pemstatus ( toObj, 'Controls', 5 )
							lnCnt = 1
							Do While Vartype ( loObj ) # 'O' And lnCnt <= toObj.Controls.Count
								loControl = toObj.Controls[ lnCnt ]
								If Pemstatus ( loControl, 'Name', 5 ) And Lower ( loControl.Name ) == tcObj
									loObj = loControl

								Else
									loObj = This.GetRefR ( tcObj, loControl )

								Endif && Pemstatus( loControl, 'Name', 5 ) And Lower( loControl.Name ) == tcObj
								lnCnt = lnCnt + 1

							Enddo

						Endif && Pemstatus( toObj, 'Controls', 5 )

						If Pemstatus ( toObj, 'Objects', 5 )
							lnCnt = 1
							Do While Vartype ( loObj ) # 'O' And lnCnt <= toObj.Objects.Count
								loControl = toObj.Objects[ lnCnt ]
								If Pemstatus ( loControl, 'Name', 5 ) And Lower ( loControl.Name ) == tcObj
									loObj = loControl

								Else
									loObj = This.GetRefR ( tcObj, loControl )

								Endif && Pemstatus( loControl, 'Name', 5 ) And Lower( loControl.Name ) == tcObj
								lnCnt = lnCnt + 1

							Enddo

						Endif && Pemstatus( toObj, 'Objects', 5 )

				Endcase

			Endif && Vartype( toObj ) = 'O'

		Catch To loErr
			loObj = Null
			loError = This.GetOriginalError ( loErr )
			loError.Message = 'GetRefR' + CR ;
				+ TB + 'tcObj: ' + tcObj + CR ;
				+ TB + 'toObj: ' + toObj.Name + CR + loError.Message

			Throw loError

		Finally
			loError = Null

		Endtry

		Return loObj

	Endfunc && GetRefR

	* GetTempPath
	Protected Function GetTempPath() As String

		Local lcTmp As String

		lcTmp = Getenv ( 'TMP' )
		If ! This.ValidPath ( lcTmp )
			lcTmp = Getenv ( 'TEMP' )
			If ! This.ValidPath ( lcTmp )
				lcTmp = Justpath ( Sys ( 16 ) )

			Endif

		Endif

		Return lcTmp

	Endfunc && GetTempPath

	* ValidPath
	Protected Function ValidPath ( tcPath As String ) As Boolean

		Return ! Empty ( tcPath ) And Directory ( tcPath, 1 )

	Endfunc && ValidPath

	* HandlerEvent
	Procedure HandlerEvent ( tv1 As variant, tv2 As variant, tv3 As variant, ;
			tv4 As variant, tv5 As variant, tv6 As variant, tv7 As variant, ;
			tv8 As variant, tv9 As variant, tv10 As variant, tv11 As variant, ;
			tv12 As variant, tv13 As variant, tv14 As variant, tv15 As variant, ;
			tv16 As variant, tv17 As variant, tv18 As variant, tv19 As variant, ;
			tv20 As variant, tv21 As variant, tv22 As variant, tv23 As variant, ;
			tv24 As variant, tv25 As variant, tv26 As variant )

		Local l, ;
			laEvents[1], ;
			lcCode As String, ;
			lcCreateParam As String, ;
			lcEvent As String, ;
			lcFile As String, ;
			lcMessage As String, ;
			lcObjPath As String, ;
			liIdx As Integer, ;
			liJdx As Integer, ;
			liKdx As Integer, ;
			liLdx As Integer, ;
			lnEvtCnt As Number, ;
			lnPcount As Number, ;
			loControl As Object, ;
			loErr As Exception, ;
			loError As Exception, ;
			loEvent As Object, ;
			loEventDeFaultParam As Object, ;
			loForm As Form, ;
			loFormSet As Formset, ;
			loObj As Object, ;
			loParam As Object, ;
			loSender As Object

		Try
			If ! This.lOnExit
				lnEvtCnt = Aevents ( laEvents, 0 )
				lcCode = ''
				lcCreateParam = ''

				If ! Empty ( lnEvtCnt )

					lcEvent = Lower ( laEvents[ 1, 2 ] )
					liIdx =	Thisform.oColEvents.GetKey ( lcEvent )
					If ! Empty ( liIdx )
						loEvent = Thisform.oColEvents.Item[ liIdx ]
						* Obtendo el Origen del evento
						loSender = laEvents[ 1, 1 ]
						lcObjPath = This.GetPath ( loSender )
						liJdx = loEvent.GetKey ( Lower ( lcObjPath ) )
						If ! Empty ( liJdx )
							loControl = loEvent.Item[ liJdx ]
							liLdx = This.oColDefaultParameters.GetKey ( lcEvent )
							If ! Empty ( liLdx )
								loEventDeFaultParam = This.oColDefaultParameters.Item[ liLdx ]

							Endif

							lnPcount = Pcount()

							For liKdx = 1 To loControl.Count
								lcFile = loControl.Item[ liKdx ]

								If ! Empty ( lcFile )
									loStatus = This.oColFileCompileStatus.Item[ lcFile ]
									If loStatus.lCompiled = .T.
										loForm = Thisform
										Try
											loFormSet = Thisformset
										Catch
											loFormSet = _Screen
										Endtry

										Do Case
											Case lnPcount = 1
												Do ( lcFile ) With loFormSet, loForm, loSender, tv1

											Case lnPcount = 2
												Do ( lcFile ) With loFormSet, loForm, loSender, tv1, tv2

											Case lnPcount = 3
												Do ( lcFile ) With loFormSet, loForm, loSender, tv1, tv2, tv3

											Case lnPcount = 4
												Do ( lcFile ) With loFormSet, loForm, loSender, tv1, tv2, tv3, tv4

											Case lnPcount = 5
												Do ( lcFile ) With loFormSet, loForm, loSender, tv1, tv2, tv3, tv4, tv5

											Case lnPcount = 6
												Do ( lcFile ) With loFormSet, loForm, loSender, tv1, tv2, tv3, tv4, tv5, tv6

											Case lnPcount = 7
												Do ( lcFile ) With loFormSet, loForm, loSender, tv1, tv2, tv3, tv4, tv5, tv6, tv7

											Case lnPcount = 8
												Do ( lcFile ) With loFormSet, loForm, loSender, tv1, tv2, tv3, tv4, tv5, tv6, tv7, tv8

											Case lnPcount = 9
												Do ( lcFile ) With loFormSet, loForm, loSender, tv1, tv2, tv3, tv4, tv5, tv6, tv7, tv8, tv9

											Case lnPcount = 10
												Do ( lcFile ) With loFormSet, loForm, loSender, tv1, tv2, tv3, tv4, tv5, tv6, tv7, tv8, tv9, tv10

											Case lnPcount = 11
												Do ( lcFile ) With loFormSet, loForm, loSender, tv1, tv2, tv3, tv4, tv5, tv6, tv7, tv8, tv9, tv10, tv11

											Case lnPcount = 12
												Do ( lcFile ) With loFormSet, loForm, loSender, tv1, tv2, tv3, tv4, tv5, tv6, tv7, tv8, tv9, tv10, tv11, tv12

											Case lnPcount = 13
												Do ( lcFile ) With loFormSet, loForm, loSender, tv1, tv2, tv3, tv4, tv5, tv6, tv7, tv8, tv9, tv10, tv11, tv12, tv13

											Case lnPcount = 14
												Do ( lcFile ) With loFormSet, loForm, loSender, tv1, tv2, tv3, tv4, tv5, tv6, tv7, tv8, tv9, tv10, tv11, tv12, tv13, tv14

											Case lnPcount = 15
												Do ( lcFile ) With loFormSet, loForm, loSender, tv1, tv2, tv3, tv4, tv5, tv6, tv7, tv8, tv9, tv10, tv11, tv12, tv13, tv14, tv15

											Case lnPcount = 16
												Do ( lcFile ) With loFormSet, loForm, loSender, tv1, tv2, tv3, tv4, tv5, tv6, tv7, tv8, tv9, tv10, tv11, tv12, tv13, tv14, tv15, tv16

											Case lnPcount = 17
												Do ( lcFile ) With loFormSet, loForm, loSender, tv1, tv2, tv3, tv4, tv5, tv6, tv7, tv8, tv9, tv10, tv11, tv12, tv13, tv14, tv15, tv16, tv17

											Case lnPcount = 18
												Do ( lcFile ) With loFormSet, loForm, loSender, tv1, tv2, tv3, tv4, tv5, tv6, tv7, tv8, tv9, tv10, tv11, tv12, tv13, tv14, tv15, tv16, tv17, tv18

											Case lnPcount = 19
												Do ( lcFile ) With loFormSet, loForm, loSender, tv1, tv2, tv3, tv4, tv5, tv6, tv7, tv8, tv9, tv10, tv11, tv12, tv13, tv14, tv15, tv16, tv17, tv18, tv19

											Case lnPcount = 20
												Do ( lcFile ) With loFormSet, loForm, loSender, tv1, tv2, tv3, tv4, tv5, tv6, tv7, tv8, tv9, tv10, tv11, tv12, tv13, tv14, tv15, tv16, tv17, tv18, tv19, tv20

											Case lnPcount = 21
												Do ( lcFile ) With loFormSet, loForm, loSender, tv1, tv2, tv3, tv4, tv5, tv6, tv7, tv8, tv9, tv10, tv11, tv12, tv13, tv14, tv15, tv16, tv17, tv18, tv19, tv20, tv21

											Case lnPcount = 22
												Do ( lcFile ) With loFormSet, loForm, loSender, tv1, tv2, tv3, tv4, tv5, tv6, tv7, tv8, tv9, tv10, tv11, tv12, tv13, tv14, tv15, tv16, tv17, tv18, tv19, tv20, tv21, tv22

											Case lnPcount = 23
												Do ( lcFile ) With loFormSet, loForm, loSender, tv1, tv2, tv3, tv4, tv5, tv6, tv7, tv8, tv9, tv10, tv11, tv12, tv13, tv14, tv15, tv16, tv17, tv18, tv19, tv20, tv21, tv22, tv23

											Case lnPcount = 24
												Do ( lcFile ) With loFormSet, loForm, loSender, tv1, tv2, tv3, tv4, tv5, tv6, tv7, tv8, tv9, tv10, tv11, tv12, tv13, tv14, tv15, tv16, tv17, tv18, tv19, tv20, tv21, tv22, tv23, tv24

											Case lnPcount = 25
												Do ( lcFile ) With loFormSet, loForm, loSender, tv1, tv2, tv3, tv4, tv5, tv6, tv7, tv8, tv9, tv10, tv11, tv12, tv13, tv14, tv15, tv16, tv17, tv18, tv19, tv20, tv21, tv22, tv23, tv24, tv25

											Case lnPcount = 26
												Do ( lcFile ) With loFormSet, loForm, loSender, tv1, tv2, tv3, tv4, tv5, tv6, tv7, tv8, tv9, tv10, tv11, tv12, tv13, tv14, tv15, tv16, tv17, tv18, tv19, tv20, tv21, tv22, tv23, tv24, tv25, tv26

											Otherwise
												Do ( lcFile ) With loFormSet, loForm, loSender

										Endcase

									Endif && loStatus.lCompiled = .T

								Endif && ! Empty ( lcFile )

							Endfor

						Endif && ! Empty( liJdx )

					Endif && ! Empty( liIdx )

				Endif

			Endif && ! this.lOnExit

		Catch To loErr
			loObj = Null

			loError = This.GetOriginalError ( loErr )

			loError.Message = Transform ( Datetime() ) + CR ;
				+ 'HandlerEvent' + CR ;
				+ TB + 'tv1: ' + Transform ( tv1 ) + CR ;
				+ TB + 'tv2: ' + Transform ( tv2 ) + CR ;
				+ TB + 'tv3: ' + Transform ( tv3 ) + CR ;
				+ TB + 'tv4: ' + Transform ( tv4 ) + CR ;
				+ TB + 'tv5: ' + Transform ( tv5 ) + CR ;
				+ TB + 'tv6: ' + Transform ( tv6 ) + CR ;
				+ TB + 'tv7: ' + Transform ( tv7 ) + CR ;
				+ TB + 'tv8: ' + Transform ( tv8 ) + CR ;
				+ TB + 'tv9: ' + Transform ( tv9 ) + CR ;
				+ TB + 'tv10: ' + Transform ( tv10 ) + CR ;
				+ TB + 'tv11: ' + Transform ( tv11 ) + CR ;
				+ TB + 'tv12: ' + Transform ( tv12 ) + CR ;
				+ TB + 'tv13: ' + Transform ( tv13 ) + CR ;
				+ TB + 'tv14: ' + Transform ( tv14 ) + CR ;
				+ TB + 'tv15: ' + Transform ( tv15 ) + CR ;
				+ TB + 'tv16: ' + Transform ( tv16 ) + CR ;
				+ TB + 'tv17: ' + Transform ( tv17 ) + CR ;
				+ TB + 'tv18: ' + Transform ( tv18 ) + CR ;
				+ TB + 'tv19: ' + Transform ( tv19 ) + CR ;
				+ TB + 'tv20: ' + Transform ( tv20 ) + CR ;
				+ TB + 'tv21: ' + Transform ( tv21 ) + CR ;
				+ TB + 'tv22: ' + Transform ( tv22 ) + CR ;
				+ TB + 'tv23: ' + Transform ( tv23 ) + CR ;
				+ TB + 'tv24: ' + Transform ( tv24 ) + CR ;
				+ TB + 'tv25: ' + Transform ( tv25 ) + CR ;
				+ TB + 'tv26: ' + Transform ( tv26 ) + CR ;
				+ loError.Message + CR ;
				+ 'Codigo: ' + lcCode

			lcMessage = This.GetExceptionString ( loError )
			Strtofile ( CR + lcMessage, Forceext ( This.Name, 'Err' ), 1 )

		Finally
			loControl = Null
			loErr = Null
			loError = Null
			loEvent = Null
			loEventDeFaultParam = Null
			loForm = Null
			loFormSet = Null
			loObj = Null
			loParam = Null
			loSender = Null

		Endtry

		Return

	Endproc && HandlerEvent

	* Init
	Procedure Init() As VOID
		* Inicia la colecci�n para almacenar las referencias de los archivos.
		This.oColTempEventCodeFiles = Createobject ( 'Collection' )
		This.oColFileToCompile = Createobject ( 'Collection' )
		This.oColFileCompileStatus = Createobject ( 'Collection' )
		This.cSetProc = Set ( 'Procedure' )
		* Thisform.LockScreen = .T.

	Endproc

	* oColDefaultParameters_Access
	Protected Function oColDefaultParameters_Access() As Collection
		Local loErr As Object, ;
			loError As Exception
		Try
			With This As oForm Of Pruebas\EditFrm.prg
				If Vartype ( .oColDefaultParameters ) # 'O'
					.oColDefaultParameters = Createobject ( 'Collection' )

					.FillDefaultParameters()

				Endif && Vartype( .oColDefaultParameters ) # 'O'

			Endwith

		Catch To loErr
			loError = This.GetOriginalError ( loErr )
			loError.Message = 'oColDefaultParameters_Access' + CR + loError.Message
			Throw loError

		Finally
			loError = Null

		Endtry

		Return This.oColDefaultParameters

	Endfunc && oColDefaultParameters_Access

	* oColEvents_Access
	Protected Function oColEvents_Access() As Collection

		Local loErr As Object, ;
			loError As Exception

		Try
			With This As oForm Of Pruebas\EditFrm.prg
				If Vartype ( .oColEvents ) # 'O'
					.oColEvents = Createobject ( 'Collection' )

				Endif && Vartype( .oColEvents ) # 'O'

			Endwith

		Catch To loErr
			loError = This.GetOriginalError ( loErr )
			loError.Message = 'oColEvents_Access' + CR + loError.Message
			Throw loError

		Finally
			loError = Null

		Endtry

		Return This.oColEvents

	Endfunc && oColEvents_Access

	* PrepareCode
	Protected Function PrepareCode ( tcEvent As String, tcCode As String )

		Local lcCode As String, ;
			lcParamsCall As String, ;
			lcParamsDef As String, ;
			liIdx As Integer, ;
			liJdx As Integer, ;
			liParamCnt As Integer, ;
			lluseParams As Boolean, ;
			loErr As Object, ;
			loError As Exception, ;
			loEventDeFaultParam As Object

		Try

			If Empty ( tcEvent )
				tcEvent = ''

			Endif

			liIdx = This.oColDefaultParameters.GetKey ( Lower ( tcEvent ) )
			If ! Empty ( liIdx )
				loEventDeFaultParam = This.oColDefaultParameters.Item ( liIdx )
				liParamCnt = loEventDeFaultParam.Count

			Else
				liParamCnt = 0

			Endif && ! Empty ( liIdx )

			lcParamsDef = ''
			lcParamsCall = ''
			lluseParams = .F.
			For liJdx = 1 To liParamCnt
				lcParamsDef = lcParamsDef + 'tv' + Transform ( liJdx ) + ' As Variant'
				lcParamsCall = lcParamsCall + 'm.tv' + Transform ( liJdx )
				If liJdx < liParamCnt
					lcParamsDef = lcParamsDef + ', '
					lcParamsCall = lcParamsCall + ', '

				Endif && liJdx < liParamCnt

				lluseParams = .T.

			Next

			lcCode = ''

			TEXT To lcCode Additive Textmerge Noshow
LParameters toThisformSet As Object, toThisform As Object, toSender As Object <<IIF( lluseParams, ',' + lcParamsDef , '' )>>

#Define ToDebugOut Debugout Time(0), Program(), m.loErr.Message, m.loErr.Details, m.loErr.ErrorNo, m.loErr.LineContents, m.loErr.StackLevel, m.loErr.Procedure, m.loErr.Lineno
#Define ToDebugOutCls Debugout Time(0), Program(), m.loErr.Message, m.loErr.Details, m.loErr.ErrorNo, m.loErr.LineContents, m.loErr.StackLevel, This.Class + '.' + m.loErr.Procedure, This.ClassLibrary, m.loErr.Lineno

Private poSender, poThisform
Local loErr as Exception

Try

	With toThisformSet

		With toThisform

			* This reference Hack.
			* Use a private var for access to the control
			* who triggered the event with the This Keyword.
			poSender = toSender
			With poSender


					* Thisform reference Hack.
					* Add a wrapper class to a Form with the dynamic
					* code for access to the current form with ThisForm keyword.
					toThisform.AddObject( '__CtlDynamicProxy<<tcEvent>>', 'DynamicProxy<<tcEvent>>' )

					* Thisform reference Hack.
					* Add a wrapper class to a Form with the dynamic
					* code for access to the current form with ThisForm keyword.
					* AddProperty( toThisformSet, '__CtlDynamicProxy<<tcEvent>>', toThisform.__CtlDynamicProxy<<tcEvent>> )

					* Call a dynamicCode for event <<tcEvent>>
					toThisform.__CtlDynamicProxy<<tcEvent>>.DynamicWrapperMethod(<<lcParamsCall>>)

				EndWith && toSender

		EndWith && toThisform

	EndWith && toThisformSet

Catch To loErr
	ToDebugOut, '<<tcEvent>>' <<IIF( lluseParams, ',' + lcParamsCall, '' )>>
	Throw

Finally
	poSender = Null

	Try
		If Type( 'toThisformSet' ) = 'O' And Vartype( toThisformSet) = 'O' And PemStatus( toThisformSet, '__CtlDynamicProxy<<tcEvent>>', 5 )
			RemoveProperty( toThisformSet, '__CtlDynamicProxy<<tcEvent>>' )
		EndIf && Type( 'toThisformSet' ) = 'O' And Vartype( toThisformSet) = 'O' And PemStatus( toThisformSet, '__CtlDynamicProxy<<tcEvent>>', 5 )
	Catch
	EndTry

	Try
		If Type( 'toThisform' ) = 'O' And Vartype( toThisform ) = 'O' And PemStatus( toThisform, '__CtlDynamicProxy<<tcEvent>>', 5 )
			toThisform.RemoveObject( '__CtlDynamicProxy<<tcEvent>>' )
		EndIf && Type( 'toThisform' ) = 'O' And Vartype( toThisform ) = 'O' And PemStatus( toThisform, '__CtlDynamicProxy<<tcEvent>>', 5 )
	Catch
	EndTry

EndTry

* DynamicProxy<<tcEvent>>
Define Class DynamicProxy<<tcEvent>> As Custom

	* This_Access
	Protected Procedure This_Access( tcMember as String ) As Object

		Local loRet as Object

		* FIX this reference.
		Try
			If lower( tcMember )  $ 'dynamicwrappermethod'
				loRet = this

			Else
				loRet = poSender

			EndIf

		Catch To loErr
			ToDebugOutCls, '<<tcEvent>>'
			Throw

		EndTry

		Return loRet

	EndProc && This_Access

	* DynamicWrapperMethod
	Procedure DynamicWrapperMethod
		<<IIF( lluseParams And ! Atc( 'param', tcCode ) > 0, 'LParameters ' + lcParamsDef, '' )>>

		Try

			ENDTEXT

			lcCode = lcCode + Chr ( 13 ) + Iif ( ! Empty ( tcCode ), tcCode, 'Wait Window "Sin codigo" NoWait') + Chr ( 13 )  + Chr ( 13 )

			TEXT To lcCode Additive Textmerge Noshow

		Catch To loErr
			ToDebugOutCls, '<<tcEvent>>' <<IIF( lluseParams, ',' + lcParamsCall, '' )>>
			Throw

		EndTry

Enddefine && DynamicProxy<<tcEvent>>

			ENDTEXT

		Catch To loErr
			loError = This.GetOriginalError ( loErr )
			loError.Message = 'PrepareCode' + CR ;
				+ TB + 'tcCode: ' + tcCode + CR + loError.Message

			Throw loError

		Finally
			loError = Null

		Endtry

		Return lcCode

	Endfunc && PrepareCode

	* QueryUnload
	Procedure QueryUnload() As VOID
		This.lOnExit = .T.
		This.ClearBindEvents()
		DoDefault()

	Endproc && QueryUnload

	* Unload
	Procedure Unload()
		If This.WindowType = 1
			Return This.xReturn

		Endif

	Endproc && Unload

Enddefine && oFormEventHandler

* DynamicEventsForm
Define Class DynamicEventsForm As DynamicEventsHandler Of DynamicEvents.prg
	#If .F.
		Local This As DynamicEventsForm Of DynamicEvents.prg
	#Endif

	_MemberData = [<?xml version="1.0" encoding="Windows-1252" standalone="yes"?>] + ;
		[<VFPData>] + ;
		[<memberdata name="loadconfig" type="method" display="LoadConfig" />] + ;
		[<memberdata name="loadcontrols" type="method" display="LoadControls" />] + ;
		[<memberdata name="loadproperties" type="method" display="LoadProperties" />] + ;
		[<memberdata name="loadmethods" type="method" display="LoadMethods" />] + ;
		[<memberdata name="addobjectto" type="method" display="AddObjectTo" />] + ;
		[<memberdata name="applyproperty" type="method" display="ApplyProperty" />] + ;
		[<memberdata name="applypropertiesto" type="method" display="ApplyPropertiesTo" />] + ;
		[</VFPData>]

	* AddObjectTo
	Procedure AddObjectTo ( tcControlName As String, ;
			tcObjParent As String, ;
			tcObjClass As String, ;
			tcObjClassLibrary As String, ;
			tcProperties As String, ;
			tcCursorAlias As String )

		Local lcAlias As String, ;
			lcCommand As String, ;
			lcControlName As String, ;
			lcObjClass As String, ;
			lcObjClassLibrary As String, ;
			lcObjParent As String, ;
			lcProperties As String, ;
			loErr As Object, ;
			loError As Object, ;
			loObj As Object, ;
			loObjParent As Object

		Try
			lcAlias = Alias()

			If ! Empty ( tcObjParent )
				tcObjParent = Alltrim ( tcObjParent )
				loObjParent = This.GetRef ( tcObjParent )

				If Vartype ( loObjParent ) # 'O'
					Select Alias ( tcCursorAlias )
					TEXT To lcCommand Noshow Textmerge Pretext 15
						Locate for Lower( Alltrim( cObjName ) ) = '<< Lower( tcObjParent  )>>'
					ENDTEXT

					&lcCommand.

					lcControlName = Evaluate ( tcCursorAlias + '.cObjName' )
					lcObjParent = Evaluate ( tcCursorAlias + '.cParent' )
					lcObjClass = Evaluate ( tcCursorAlias + '.cClass' )
					lcObjClassLibrary = Evaluate ( tcCursorAlias + '.cClassLib' )
					lcProperties = Evaluate ( tcCursorAlias + '.cProps' )

					This.AddObjectTo ( lcControlName, lcObjParent, lcObjClass, lcObjClassLibrary, lcProperties, tcCursorAlias )

					loObjParent = This.GetRef ( tcObjParent )

				Endif && Vartype( loObjParent ) # 'O'

			Else
				loObjParent = Thisform

			Endif && ! Empty( lcObjParent )

			If ! Empty ( tcObjClassLibrary )
				tcObjClassLibrary = Lower ( Alltrim ( tcObjClassLibrary ) )
				If  ! ( tcObjClassLibrary $ Lower ( Set ('Classlib' ) ) ) Or ! ( tcObjClassLibrary $ Lower ( Set ('Procedure') ) )

					Do Case
						Case Inlist ( Justext ( tcObjClassLibrary ), 'prg', 'fxp' )

							TEXT To lcCommand Noshow Textmerge Pretext 15
								Set Procedure To '<<tcObjClassLibrary>>' Additive

							ENDTEXT

						Case Inlist ( Justext ( tcObjClassLibrary ), 'vcx' )
							TEXT To lcCommand Noshow Textmerge Pretext 15
								Set Classlib To '<<tcObjClassLibrary>>' Additive

							ENDTEXT

						Otherwise
							Error 'Tipo de archivo invalido para ClassLibrary'

					Endcase

					&lcCommand.

				Endif && ! ( tcObjClassLibrary $ Lower( Set("Classlib" ) ) ) Or ! ( tcObjClassLibrary $ Lower( Set("Procedure") ) )

			Endif && ! Empty( tcObjClassLibrary )

			tcControlName = Alltrim ( tcControlName )
			tcObjClass = Alltrim ( tcObjClass )

			Assert Vartype ( loObjParent ) = 'O' Message 'loObjParent noes un Objeto'

			loObjParent.AddObject ( tcControlName, tcObjClass )

			loObj = loObjParent.&tcControlName.
			TEXT To lcCommand Noshow Textmerge Pretext 15
				Replace Loaded with .T.
				For Lower( Alltrim( cObjName ) ) == '<<Lower( Alltrim( tcControlName ) )>>'
				And Lower( Alltrim( cParent ) ) == '<<Lower( Alltrim( tcObjParent ) )>>'
				in <<tcCursorAlias>>
			ENDTEXT

			&lcCommand.

			This.ApplyPropertiesTo ( loObj, tcProperties )

		Catch To loErr
			loError = This.GetOriginalError ( loErr )
			loError.Message = 'AddObjectTo' + CR ;
				+ 'tcControlName: ' + Transform ( tcControlName ) + CR ;
				+ 'tcObjParent: ' + Transform ( tcObjParent ) + CR ;
				+ 'tcObjClass: ' + Transform ( tcObjClass ) + CR ;
				+ 'tcObjClassLibrary: ' + Transform ( tcObjClassLibrary ) + CR ;
				+ 'tcProperties: ' + Transform ( tcProperties ) + CR ;
				+ 'tcCursorAlias: ' + Transform ( tcCursorAlias ) + CR ;
				+ loErr.Message

			Throw loError

		Finally
			loError = Null
			loObj = Null
			If Used ( lcAlias )
				Select Alias ( lcAlias )

			Endif && Used( lcAlias )

		Endtry

	Endproc && AddObjectTo

	* ApplyProperty
	Protected Procedure ApplyProperty ( toObj As Object, tcProperty As String ) As VOID

		Local lcProp As String, ;
			lcValue As String, ;
			loErr As Object, ;
			loError As Object, ;
			lvValue As variant

		Try
			lcProp = Alltrim ( Getwordnum ( tcProperty, 1, '=' ) )
			lcValue = Alltrim ( Getwordnum ( tcProperty, 2, '=' ) )
			lvValue = &lcValue.

			If Pemstatus ( toObj, lcProp, 5 )
				toObj.&lcProp. = lvValue

			Else
				Error 'No existe la propiedad ' + lcProp + ' en el objeto ' + toObj.Name

			Endif && Pemstatus( toObj, lcProp, 5 )

		Catch To loErr
			loError = This.GetOriginalError ( loErr )
			loError.Message = 'ApplyProperty' + CR ;
				+ 'toObj: ' + toObj.Name + CR ;
				+ 'tcProperty: ' + tcProperty + CR ;
				+ loErr.Message

			Throw loErr

		Finally
			loErr = Null

		Endtry

	Endproc && ApplyProperty

	* ApplyPropertiesTo
	Protected Procedure ApplyPropertiesTo ( toObj As Object, tcProperties As String ) As VOID
		Local laLines[1], ;
			liIdx As Integer, ;
			lnCnt As Integer, ;
			loErr As Exception, ;
			loError As Object

		Try
			lnCnt = Alines ( laLines, tcProperties, 1 + 4 )

			If lnCnt > 0
				For liIdx = 1 To lnCnt
					This.ApplyProperty ( toObj, laLines[ liIdx ] )

				Endfor
			Else
				This.ApplyProperty ( toObj, tcProperties )

			Endif && lnCnt > 0

		Catch To loErr
			loError = This.GetOriginalError ( loErr )
			loError.Message = 'ApplyPropertiesTo' + CR ;
				+ 'toObj: ' + toObj.Name + CR ;
				+ 'tcProperties: ' + tcProperties + CR ;
				+ loErr.Message

			Throw loErr

		Finally
			loErr = Null

		Endtry
	Endproc && ApplyPropertiesTo

	* LoadConfig
	Procedure LoadConfig ( tcFile As String ) As VOID

		Local lcAlias As String, ;
			lcCommand As String, ;
			lcExt As String, ;
			lcMessage As String, ;
			lcTables As String, ;
			lcTblAlias As String, ;
			liIdx As Integer, ;
			lnCnt As Number, ;
			loErr As Exception, ;
			loError As Object, ;
			loTable As Xmltable, ;
			loXA As Xmladapter

		Try

			lcTables = ''
			lcAlias = Alias()
			If ! Empty ( tcFile )
				lcExt = Lower ( Justext ( tcFile ) )
				Do Case
					Case lcExt $ 'xml xcfg'
						loXA = Createobject ( 'Xmladapter' )
						loXA.LoadXML ( tcFile, .T., .T. )

						lnCnt = loXA.Tables.Count
						For liIdx = 1 To lnCnt
							loTable = loXA.Tables.Item[ liIdx ]
							lcTables = lcTables + loTable.Alias
							Use In Select ( loTable.Alias )
							loTable.ToCursor()
							If liIdx # lnCnt
								lcTables = lcTables + ','

							Endif

						Endfor

						If Used ( 'cfgControls' )
							This.LoadControls ( 'cfgControls' )

						Endif && Used( 'cfgControls' )

						If Used ( 'cfgProperties' )
							This.LoadProperties ( 'cfgProperties' )

						Endif && Used( 'cfgProperties' )

						If Used ( 'cfgMethods' )
							This.LoadMethods ( 'cfgMethods' )

						Endif && Used( 'cfgMethods' )

						* Case Inlist( lcExt, 'dbf', 'dcfg' )
					Case lcExt $ 'dbf dcfg'
						* Error 'Funcionalidad no implementada'
						Use In Select ( 'cfgConfig' )
						TEXT To lcCommand Noshow Textmerge Pretext 15
							Use '<<tcFile>>' In 0 Shared Again Alias cfgConfig

						ENDTEXT

						&lcCommand.

						If Used ( 'cfgConfig' )
							lcTables = 'cfgConfig'

							This.LoadControls ( 'cfgConfig', 'cType = "C"' )
							This.LoadProperties ( 'cfgConfig', 'cType = "P"' )
							This.LoadMethods ( 'cfgConfig', 'cType = "M"' )

						Endif
					Otherwise
						Error 'Tipo de archivo desconocido'

				Endcase

			Endif

		Catch To loErr
			loError = This.GetOriginalError ( loErr )
			loError.Message = 'LoadConfig' + CR ;
				+ 'tcFile: ' + Transform ( tcFile ) + CR + loErr.Message

			lcMessage = This.GetExceptionString ( loError )
			If Version ( 2 ) # 2
				Strtofile ( CR + lcMessage, Forceext ( This.Name, 'Err' ), 1 )

			Else
				Messagebox ( lcMessage )

			Endif && Version( 2 ) # 2

		Finally
			loErr = Null
			If ! Empty ( lcTables )

				lnCnt = Getwordcount ( lcTables, ',' )
				For liIdx = 1 To lnCnt
					lcTblAlias = Getwordnum ( lcTables, liIdx, ',' )
					Use In Select ( lcTblAlias )

				Endfor

			Endif && ! Empty( lcTables )

			If Used ( lcAlias )
				Select Alias ( lcAlias )

			Endif && Used( lcAlias )

		Endtry

	Endproc && LoadConfig

	* LoadControls
	Protected Procedure LoadControls ( tcCursorAlias As String, ;
			tcFilter As String ) As VOID

		Local lcAlias As String, ;
			lcCommand As String, ;
			lcControlName As String, ;
			lcObjClass As String, ;
			lcObjClassLibrary As String, ;
			lcObjParent As String, ;
			lcProperties As String, ;
			lnRecNo As Number, ;
			loErr As Exception, ;
			loError As Object

		Try
			lcAlias = Alias()
			If Used ( tcCursorAlias )
				If Empty ( tcFilter )
					tcFilter = '.T.'

				Endif && Empty( tcFilter )

				Select Alias ( tcCursorAlias )
				Locate

				Scan For ( &tcFilter ) ;
						And ! Deleted ( tcCursorAlias ) ;
						And ! Evaluate ( tcCursorAlias + '.Loaded' )
					lnRecNo = Recno ( tcCursorAlias )

					lcControlName = Evaluate ( tcCursorAlias + '.cObjName' )
					lcObjParent = Evaluate ( tcCursorAlias + '.cParent' )
					lcObjClass = Evaluate ( tcCursorAlias + '.cClass' )
					* Set

					lcObjClassLibrary = Evaluate ( tcCursorAlias + '.cClassLib' )
					lcProperties = Evaluate ( tcCursorAlias + '.cProps' )

					This.AddObjectTo ( lcControlName, lcObjParent, lcObjClass, lcObjClassLibrary, lcProperties, tcCursorAlias )

					TEXT To lcCommand Noshow Textmerge Pretext 15
						Locate for Recno( tcCursorAlias ) = <<lnRecNo>>
					ENDTEXT

					&lcCommand.

					If Len ( Evaluate ( tcCursorAlias + '.cEventCode' ) ) > 0
						This.LoadMethods ( tcCursorAlias, Textmerge ( 'Recno( tcCursorAlias ) = <<lnRecNo>>' ) )

						&lcCommand.

					Endif &&  Len( Evaluate( tcCursorAlias + '.cEventCode' ) ) > 0

				Endscan

			Endif && Used( tcCursorAlias )

		Catch To loErr
			loError = This.GetOriginalError ( loErr )
			loError.Message = 'LoadControls' + CR ;
				+ 'tcCursorAlias: ' + Transform ( tcCursorAlias ) + CR ;
				+ 'tcFilter: ' + Transform ( tcFilter ) + CR + loErr.Message

			Throw loError

		Finally
			loError= Null

			If Used ( lcAlias )
				Select Alias ( lcAlias )

			Endif && Used( lcAlias )

		Endtry

	Endproc && LoadControls

	* LoadMethods
	Protected Procedure LoadMethods ( tcCursorAlias As String, ;
			tcFilter As String ) As VOID

		Local lcAlias As String, ;
			lcEventName As String, ;
			lcMethodCode As String, ;
			lcObjName As String, ;
			lnPriority As Integer, ;
			loErr As Exception, ;
			loError As Object

		Try
			lcAlias = Alias()
			If Used ( tcCursorAlias )
				If Empty ( tcFilter )
					tcFilter = '.T.'

				Endif && Empty( tcFilter )

				Select Alias ( tcCursorAlias )
				Locate

				Scan For ( &tcFilter. ) And ! Deleted ( tcCursorAlias )

					lcObjName = cObjName
					lcEventName = cEventName
					lcMethodCode = cEventCode
					lnPriority = nPriority

					This.AddEventCode ( lcObjName, lcEventName, lcMethodCode, lnPriority )

				Endscan

			Endif && Used( tcCursorAlias )

		Catch To loErr
			loError = This.GetOriginalError ( loErr )
			loError.Message = 'LoadControls' + CR ;
				+ 'tcCursorAlias: ' + tcCursorAlias + CR ;
				+ 'tcFilter: ' + tcFilter + CR + loErr.Message

			Throw loErr

		Finally
			loErr = Null

			If Used ( lcAlias )
				Select Alias ( lcAlias )

			Endif && Used( lcAlias )

		Endtry

	Endproc && LoadMethods

	* LoadProperties
	Protected Procedure LoadProperties ( tcCursorAlias As String, ;
			tcFilter As String ) As VOID

		Local lcAlias As String, ;
			lcObjName As String, ;
			lcProperties As String, ;
			loErr As Exception, ;
			loError As Object, ;
			loObj As Object

		Try
			lcAlias = Alias()
			If Used ( tcCursorAlias )
				If Empty ( tcFilter )
					tcFilter = '.T.'

				Endif && Empty( tcFilter )

				Select Alias ( tcCursorAlias )
				Locate

				Scan For ( &tcFilter ) And ! Deleted ( tcCursorAlias )
					lcObjName = Evaluate ( tcCursorAlias + '.cObjName' )
					lcProperties = Evaluate ( tcCursorAlias + '.cProps' )
					loObj = This.GetRef ( lcObjName )
					Assert Vartype ( loObj ) = 'O' Message 'loObj no es un objeto'
					This.ApplyPropertiesTo ( loObj, lcProperties )

				Endscan

			Endif && Used( tcCursorAlias )

		Catch To loErr
			loError = This.GetOriginalError ( loErr )
			loError.Message = 'LoadControls' + CR ;
				+ 'tcCursorAlias: ' + tcCursorAlias + CR ;
				+ 'tcFilter: ' + tcFilter + CR + loErr.Message

			Throw loError

		Finally
			loError = Null

			If Used ( lcAlias )
				Select Alias ( lcAlias )

			Endif && Used( lcAlias )

		Endtry

	Endproc && LoadProperties

Enddefine && oFrmDynamic

* ContainerBase
Define Class ContainerBase As Container

	#If .F.
		Local This As ContainerBase Of DynamicEvents.prg
	#Endif

	nOldBorderColor = 0
	nOldBorderWidth = 0

	* MouseEnter
	Procedure MouseEnter ( tv1, tv2, tv3, tv4 )
		This.nOldBorderColor = This.BorderColor
		This.BorderColor = Rgb ( 0, 255, 0 )

		This.nOldBorderWidth = This.BorderWidth
		This.BorderWidth = 3

	Endproc

	* MouseLeave
	Procedure MouseLeave ( tv1, tv2, tv3, tv4 )
		This.BorderColor = This.nOldBorderColor
		This.BorderWidth = This.nOldBorderWidth

	Endproc

Enddefine && oContainer

* CreateObjParam
Function CreateObjParam ( tv1 As variant, tv2 As variant, tv3 As variant, tv4 As variant, ;
		tv5 As variant, tv6 As variant, tv7 As variant, tv8 As variant, ;
		tv9 As variant, tv10 As variant, tv11 As variant, tv12 As variant, ;
		tv13 As variant, tv14 As variant, tv15 As variant, tv16 As variant, ;
		tv17 As variant, tv18 As variant, tv19 As variant, tv20 As variant, ;
		tv21 As variant, tv22 As variant, tv23 As variant, tv24 As variant, ;
		tv25 As variant, tv26 As variant ) As Object

	* DAE 2009-07-31
	* Recibe los parametros de a paraes nombre de propiedad y valor
	* CreateObjParam( "Boolean", .F. )
	* CreateObjParam( "Numeric", 1 )
	* CreateObjParam( "String", "Cadena de caracteres" )
	*

	Local lcMsg As String, ;
		lcProp As String, ;
		lcPropName As String, ;
		lcValue As String, ;
		liIdx As Integer, ;
		lnPcount As Number, ;
		loErr As Object, ;
		loParam As Object, ;
		lvValue As variant

	Try
		loParam = Createobject ( 'Empty' )
		lnPcount = Pcount()
		If Mod ( lnPcount, 2 ) = 0

			For liIdx = 1 To lnPcount Step 2
				lcProp = 'tv' + Transform ( liIdx )
				lcPropName = &lcProp.
				lcValue = 'tv' + Transform ( liIdx + 1 )
				lvValue = &lcValue.
				AddProperty ( loParam, lcPropName, lvValue )

			Endfor

		Else
			lcMsg = 'La cantidad de parametros pasados a la funci�n (' + Transform ( lnPcount ) + ') deberia ser par.' + Chr ( 13 ) ;
				+ 'Ej:'+ CR ;
				+ ' CreateObjParam( "Boolean", .F. )' + CR ;
				+ ' CreateObjParam( "Numeric", 1 )' + CR ;
				+ ' CreateObjParam( "String", "Cadena de caracteres" )'
			Error lcMsg

		Endif && Mod( Pcount(), 2 ) = 0

	Catch To loErr
		Throw loErr

	Endtry

	Return loParam

Endfunc && CreateObjParam

* MD5
* This routine takes a string as input
* and returns an MD5 hash value as a string.
* The calculation in implemented using the MS Crypto API and the RSA provider.
Function MD5 (tcData)

	Local lcHashValue As String, ;
		lhHashObject As Number, ;
		lhProv As Number, ;
		lnDataSize As Number, ;
		lnErr As Number, ;
		lnHashSize As Number, ;
		lnStatus As Number, ;
		loErr As Exception

	#Include "foxpro.h"
	#Include "WinCrypt.h"

	Try

		Declare Integer GetLastError ;
			In win32api As GetLastError

		Declare Integer CryptAcquireContextA ;
			In win32api As CryptAcquireContext ;
			Integer @lhProvHandle, ;
			String  cContainer, ;
			String  cProvider, ;
			Integer nProvType, ;
			Integer nFlags

		Declare Integer CryptCreateHash ;
			In win32api As CryptCreateHash ;
			Integer hProviderHandle, ;
			Integer nALG_ID, ;
			Integer hKeyhandle, ;
			Integer nFlags, ;
			Integer @hCryptHashHandle

		Declare Integer CryptHashData ;
			In win32api As CryptHashData ;
			Integer hHashHandle, ;
			String  @cData, ;
			Integer nDataLen, ;
			Integer nFlags

		Declare Integer CryptGetHashParam ;
			In win32api As CryptGetHashParam ;
			Integer hHashHandle, ;
			Integer nParam, ;
			String  @cHashValue, ;
			Integer @nHashSize, ;
			Integer nFlags

		Declare Integer CryptDestroyHash ;
			In win32api As CryptDestroyHash ;
			Integer hKeyhandle

		Declare Integer CryptReleaseContext ;
			In win32api As CryptReleaseContext ;
			Integer hProvHandle, ;
			Integer nReserved

		lhProv = 0
		lhHashObject = 0
		lnDataSize = Len (tcData)
		lcHashValue = Replicate (Chr(0), 16)
		lnHashSize = Len (lcHashValue)

		* load a crypto provider

		lnStatus = CryptAcquireContext ( @lhProv, 0, 0, dnPROV_RSA_FULL, dnCRYPT_VERIFYCONTEXT )
		If lnStatus # 0

			* create a hash object that uses MD5 algorithm
			lnStatus = CryptCreateHash ( lhProv, dnCALG_MD5, 0, 0, @lhHashObject )
			If lnStatus # 0

				* add the input data to the hash object
				lnStatus = CryptHashData ( lhHashObject, tcData, lnDataSize, 0 )
				If lnStatus # 0

					* retrieve the hash value, if caller did not provide enough storage (16 bytes for MD5)
					* this will fail with dnERROR_MORE_DATA and lnHashSize will contain needed storage size
					lnStatus = CryptGetHashParam ( lhHashObject, dnHP_HASHVAL, @lcHashValue, @lnHashSize, 0 )
					If lnStatus # 0

						* free the hash object
						lnStatus = CryptDestroyHash ( lhHashObject )
						If lnStatus # 0

							* release the crypto provider
							lnStatus = CryptReleaseContext ( lhProv, 0 )

						Endif


					Endif

				Endif

			Endif

		Endif

		If lnStatus = 0
			Throw GetLastError()

		Endif

	Catch To loErr

		* clean up the hash object and release provider
		If lhHashObject # 0
			CryptDestroyHash ( lhHashObject )

		Endif && lhHashObject # 0

		If lhProv # 0
			CryptReleaseContext ( lhProv, 0 )

		Endif && lhProv # 0

		* Error ("HashMD5 Failed")

		Throw

	Finally
		Clear Dlls GetLastError, ;
			CryptAcquireContext, ;
			CryptCreateHash, ;
			CryptHashData, ;
			CryptGetHashParam, ;
			CryptDestroyHash, ;
			CryptReleaseContext

	Endtry

	Return Strconv ( lcHashValue, 15 )

Endfunc && MD5
